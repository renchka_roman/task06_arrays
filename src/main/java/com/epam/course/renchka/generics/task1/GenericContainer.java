package com.epam.course.renchka.generics.task1;

import java.util.ArrayList;
import java.util.List;

public class GenericContainer<T> {
  private List<T> units = new ArrayList<>();

  public void addUnit(T unit) {
    units.add(unit);
  }

  public T getUnit(int index) {
    return units.get(index);
  }

  public List<T> getAllUnits() {
    return units;
  }

  public void addAllUnits(List<? extends T> unitList) {
    units.addAll(unitList);
  }
}
