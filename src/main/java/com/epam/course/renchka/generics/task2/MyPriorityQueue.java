package com.epam.course.renchka.generics.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

public class MyPriorityQueue<E extends Comparable<? super E>> implements Queue<E> {
  private static final int MAX_ARRAY_CAPACITY = Integer.MAX_VALUE - 8;
  private static final int DEFAULT_CAPACITY = 10;
  private Object[] objArray;
  private int size;

  public MyPriorityQueue() {
    this.objArray = new Object[DEFAULT_CAPACITY];
  }

  public MyPriorityQueue(int initialCapacity) {
    if (initialCapacity >= 0) {
      this.objArray = new Object[initialCapacity];
    } else {
      throw new IllegalArgumentException("Illegal Capacity: " + size);
    }
  }

  @Override
  public boolean add(E e) {
    return offer(e);
  }

  @Override
  public boolean offer(E e) {
    if (e == null)
      throw new NullPointerException();
    int index = size;
    ensureCapacity(size + 1);
    size++;
    if (index == 0) {
      objArray[0] = e;
    } else {
      siftUp(index - 1, e);
    }
    return true;
  }

  @Override
  public E peek() {
    return size == 0 ? null : (E) objArray[0];
  }

  @Override
  public E remove() {
    E e = poll();
    if (e == null) {
      throw new NoSuchElementException();
    }
    return e;
  }

  @Override
  public E poll() {
    if (size == 0) {
      return null;
    }
    E e = (E)objArray[0];
    for (int i = 0; i < size - 1 ; i++) {
      objArray[i] = objArray[i + 1];
    }
    size--;
    return e;
  }

  @Override
  public E element() {
    E e = peek();
    if (e == null) {
      throw new NoSuchElementException();
    }
    return e;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean contains(Object o) {
    if (o != null) {
      for (int i = 0; i < size; i++) {
        if (objArray[i].equals(o)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public Iterator<E> iterator() {
    List<E> list = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      list.add((E)objArray[i]);
    }
    return list.iterator();
  }

  @Override
  public Object[] toArray() {
    return Arrays.copyOf(objArray, size);
  }

  @Override
  public <T> T[] toArray(T[] a) {
    if (a.length < objArray.length) {
      return (T[]) Arrays.copyOf(objArray, size, a.getClass());
    }
    System.arraycopy(objArray, 0, a, 0, size);
    if (a.length > objArray.length) {
      a[objArray.length] = null;
    }
    return a;
  }

  @Override
  public boolean remove(Object o) {
    int i = 0;
    boolean isPresent = false;
    while (i < size ) {
      isPresent = (o == null ? objArray[i] == null : o.equals(objArray[i]));
      if (isPresent) {
        break;
      }
      i++;
    }
    while (i < size - 1) {
      objArray[i] = objArray[i + 1];
      i++;
    }
    size--;
    return isPresent;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    for (Object obj: c) {
      if(!contains(obj)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    for (Object obj: c) {
      add((E)obj);
    }
    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    int oldSize = size;
    for (Object obj: c) {
      remove(obj);
    }
    return oldSize - size > 0;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    int oldSize = size;
    for (int i = size - 1; i >= 0; i--) {
      Object obj = objArray[i];
      if (!c.contains(obj)) {
        remove(obj);
      }
    }
    return oldSize - size > 0;
  }

  @Override
  public void clear() {
    size = 0;
    objArray = new Object[DEFAULT_CAPACITY];
  }

  private void ensureCapacity(int minCapacity) {
    if (minCapacity < 0)
      throw new OutOfMemoryError();
    if (minCapacity - objArray.length > 0) {
      grow(minCapacity);
    }
  }

  private void grow(int minCapacity) {
    int newCapacity = 0;
    if (minCapacity < DEFAULT_CAPACITY) {
      newCapacity = DEFAULT_CAPACITY;
    } else {
      int oldCapacity = objArray.length;
      newCapacity = (int) (oldCapacity * 1.5);
      if (newCapacity < 0) {
        newCapacity = minCapacity;
      }
      if (newCapacity - MAX_ARRAY_CAPACITY > 0) {
        newCapacity = hugeCapacity(minCapacity);
      }
    }
    objArray = Arrays.copyOf(objArray, newCapacity);
  }

  private static int hugeCapacity(int minCapacity) {
    return (minCapacity > MAX_ARRAY_CAPACITY) ?
        Integer.MAX_VALUE :
        MAX_ARRAY_CAPACITY;
  }

  private void siftUp(int index, E e) {
    while (index >= 0){
      E obj = (E)objArray[index];
      if(e.compareTo(obj) >= 0) {
        break;
      }
      objArray[index + 1] = objArray[index];
      index--;
    }
    objArray[index + 1] = e;
  }

  @Override
  public String toString() {
    if (size == 0) {
      return "";
    }
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < size; i++) {
      builder.append(objArray[i]);
      builder.append(", ");
    }
    builder.delete(builder.length() - 2, builder.length());
    return builder.toString();
  }
}
