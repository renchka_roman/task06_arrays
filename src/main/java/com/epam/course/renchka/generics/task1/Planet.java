package com.epam.course.renchka.generics.task1;

public class Planet {
  private final int id = count++;
  private static int count;

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + "-" + id;
  }
}
