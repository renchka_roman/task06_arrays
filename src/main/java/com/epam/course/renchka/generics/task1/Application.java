package com.epam.course.renchka.generics.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Application {

  public static void main(String[] args) {
    GenericContainer<Planet> planetContainer = new GenericContainer<>();

    Planet planet1 = new Planet();
    Planet planet2 = new Planet();
    List<Planet> planetList = new ArrayList<>(Arrays.asList(planet1, planet2));
    planetContainer.addAllUnits(planetList);

    Earth earth1 = new Earth();
    Earth earth2 = new Earth();
    List<Earth> earths = new ArrayList<>(Arrays.asList(earth1, earth2));
    planetContainer.addAllUnits(earths);

    Mars mars = new Mars();
    planetContainer.addUnit(mars);

    planetList = planetContainer.getAllUnits();
    System.out.println(planetList);

    Planet planet = planetContainer.getUnit(2);
    System.out.println(planet);
  }
}
