package com.epam.course.renchka.arrays.logic.game.view;

import java.io.IOException;

public interface View {

  void start() throws IOException;

  void openRandomDoor();

  void printInfoAboutTheDoors();

  void printCountOfFatalDoors();

  void printDoorsInThatOrderYouNeedToOpen();
}
