package com.epam.course.renchka.arrays.task3;

public interface Generator<T> {
  T next();
}
