package com.epam.course.renchka.arrays.logic.game.bean;

public class Artifact implements Unknown {

  public static final int MAX_POWER = 80;
  public static final int MIN_POWER = 10;

  private int power;

  public Artifact(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  @Override
  public int action() {
    return power;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName().toUpperCase();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Artifact artifact = (Artifact) o;
    return power == artifact.power;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result += 31 * result + power;
    return result;
  }
}
