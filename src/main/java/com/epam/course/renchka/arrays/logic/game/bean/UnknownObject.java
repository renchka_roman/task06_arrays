package com.epam.course.renchka.arrays.logic.game.bean;

public enum UnknownObject {
  ARTIFACT,
  MONSTER
}
