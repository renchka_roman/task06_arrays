package com.epam.course.renchka.arrays.logic.game.model;

import com.epam.course.renchka.arrays.logic.game.bean.Artifact;
import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Hero;
import com.epam.course.renchka.arrays.logic.game.bean.Monster;
import com.epam.course.renchka.arrays.logic.game.bean.Unknown;
import com.epam.course.renchka.arrays.logic.game.bean.UnknownObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Hall {

  private static final int DOORS_COUNT = 10;
  private static Random random = new Random();
  private Door[] doors;
  private Hero hero;

  public Hall() {
    this.hero = new Hero(25);
    doors = new Door[DOORS_COUNT];
  }

  void fillDoors() {
    for (int i = 0; i < doors.length; i++) {
      int number = random.nextInt(UnknownObject.values().length);
      UnknownObject unknownObject = UnknownObject.values()[number];
      doors[i] = new Door(i, createUnknownObject(unknownObject));
    }
  }

  private Unknown createUnknownObject(UnknownObject unknownObject) {
    Unknown unknown = null;
    switch (unknownObject) {
      case ARTIFACT:
        unknown = new Artifact(
            random.nextInt(
                Artifact.MAX_POWER - Artifact.MIN_POWER) + Artifact.MIN_POWER);
        break;
      case MONSTER:
        unknown = new Monster(
            random.nextInt(
                Monster.MAX_POWER - Monster.MIN_POWER) + Monster.MIN_POWER);
    }
    return unknown;
  }

  public int fight(Monster monster) {
    hero.setPower(hero.getPower() - monster.getPower());
    return hero.getPower();
  }

  public int fakeFight(Monster monster) {
    return hero.getPower() - monster.getPower();
  }

  public Door[] getDoors() {
    return doors;
  }

  public List<Door> getClosedDoors() {
    List<Door> closeDoors = new ArrayList<>();
    for (Door door : doors) {
      if (!door.isOpen()) {
        closeDoors.add(door);
      }
    }
    return closeDoors;
  }

  public Hero getHero() {
    return hero;
  }

  public Door getRandomDoor() {
    return doors[random.nextInt(doors.length)];
  }

  public boolean isPossibleToWin(int[] openOrder) {
    boolean isAlive = true;
    int power = hero.getPower();
    for (int i : openOrder) {
      Door door = doors[i];
      power += door.look().action();
      if (power < 0) {
        isAlive = false;
        break;
      }
    }
    return isAlive;
  }
}
