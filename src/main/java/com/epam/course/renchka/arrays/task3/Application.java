package com.epam.course.renchka.arrays.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Application {

  public static void main(String[] args) {
    Collection<Country> collection = generateCountry(20);
    Country[] arrayCountries = collection.toArray(new Country[0]);
    List<Country> listCountries = new ArrayList<>(collection);

    Arrays.sort(arrayCountries);
    Collections.sort(listCountries);

    printText("Array sorted by countries");
    print(arrayCountries);
    printText("List sorted by countries");
    print(listCountries);

    Comparator<Country> comparator = new CountryComparatorByCapitals();
    Arrays.sort(arrayCountries, comparator);
    listCountries.sort(comparator);

    printText("Array sorted by capitals");
    print(arrayCountries);
    printText("List sorted by capitals");
    print(listCountries);

    Country country = listCountries.get(10);
    printText("Binary search: " + country);
    System.out.println("Arrays binary search: " +
        Arrays.binarySearch(arrayCountries,country, comparator ));
    System.out.println("Collections binary search: " +
        Collections.binarySearch(listCountries, country, comparator));

  }

  public static Collection<Country> generateCountry(int count) {
    List<Country> countries = new ArrayList<>();
    CountryGenerator generator = new CountryGenerator();
    for (int i = 0; i < count; i++) {
      Country country = generator.next();
      countries.add(country);
    }
    return countries;
  }

  public static void print(Collection<? extends Country> countries) {
    for (Country country: countries)
      System.out.println(country);
  }

  public static void print(Country[] countries) {
    List<Country> countryList = new ArrayList<>(Arrays.asList(countries));
    print(countryList);
  }

  public static void printText(String text) {
    System.out.println("---------------------------------------------------");
    System.out.println(text);
    System.out.println("---------------------------------------------------");
  }
}
