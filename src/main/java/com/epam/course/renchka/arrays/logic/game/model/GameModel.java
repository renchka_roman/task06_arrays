package com.epam.course.renchka.arrays.logic.game.model;

import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Hero;
import com.epam.course.renchka.arrays.logic.game.bean.Monster;
import com.epam.course.renchka.arrays.logic.game.bean.Unknown;
import java.util.ArrayList;
import java.util.List;

public class GameModel implements Model {

  private Hall hall;

  public GameModel(Hall room) {
    this.hall = room;
  }

  @Override
  public void fillDoors() {
    hall.fillDoors();
  }

  @Override
  public Hero getHero() {
    return hall.getHero();
  }

  @Override
  public Door[] getDoors() {
    return hall.getDoors();
  }

  @Override
  public List<Door> getClosedDoors() {
    return hall.getClosedDoors();
  }

  @Override
  public String openRandomDoor() {
    Door door = hall.getRandomDoor();
    while (door.isOpen() && hall.getClosedDoors().size() > 0) {
      door = hall.getRandomDoor();
    }
    Unknown unknown = door.open();
    if (unknown.action() < 0) {
      Monster monster = (Monster) unknown;
      return fight(monster);
    } else {
      Hero hero = hall.getHero();
      hero.increasePower(hero.getPower() + unknown.action());
      return "ARTIFACT \nPower: +" + unknown.getPower();
    }
  }

  @Override
  public int[] getTheWayToWinGame() {
    List<Door> closedDoors = new ArrayList<>(getClosedDoors());
    closedDoors.
        sort((door, anotherDoor) ->
            anotherDoor.look().action() - door.look().action());
    int[] doorsId = new int[closedDoors.size()];
    int i = 0;
    for (Door door : closedDoors) {
      doorsId[i++] = door.getId();
    }
    if (hall.isPossibleToWin(doorsId)) {
      return doorsId;
    }
    return new int[0];
  }

  @Override
  public int getCountOfFatalDoors() {
    Door[] doors = hall.getDoors();
    return determineCountOfFatalDoors(doors, doors.length - 1);
  }

  private int determineCountOfFatalDoors(Door[] doors, int i) {
    if (i < 0) {
      return 0;
    }
    Door door = doors[i];
    if (door.look().action() < 0 && !door.isOpen()) {
      Monster monster = (Monster) door.look();
      if (hall.fakeFight(monster) < 0) {
        return determineCountOfFatalDoors(doors, --i) + 1;
      }
    }
    return determineCountOfFatalDoors(doors, --i);
  }

  private String fight(Monster monster) {
    int powerPoint = hall.fight(monster);
    StringBuilder builder = new StringBuilder("MONSTER\n");
    builder.append("Power: ");
    builder.append(monster.getPower());
    builder.append("\nFighting -> ");
    return powerPoint >= 0 ?
        builder.append("YOU WON!!!!").toString() :
        builder.append("YOU LOST :(").toString();
  }
}
