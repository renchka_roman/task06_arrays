package com.epam.course.renchka.arrays.logic.game.bean;

public class Hero {

  private String name;
  private int power;

  public Hero(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public void increasePower(int power) {
    this.power = power;
  }
}
