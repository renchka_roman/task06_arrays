package com.epam.course.renchka.arrays.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyDeque<E> implements Deque<E> {

  private List<E> deque;

  public MyDeque(int capacity) {
    deque = new ArrayList<>(capacity);
  }

  public MyDeque() {
    deque = new ArrayList<>();
  }

  @Override
  public boolean add(E e) {
    return deque.add(e);
  }

  @Override
  public boolean remove(Object o) {
    return deque.remove(o);
  }

  @Override
  public boolean contains(Object o) {
    return deque.contains(o);
  }

  @Override
  public Iterator<E> iterator() {
    return deque.iterator();
  }

  @Override
  public int size() {
    return deque.size();
  }

  @Override
  public void addFirst(E e) {
    deque.add(0, e);
  }

  @Override
  public void addLast(E e) {
    deque.add(e);
  }

  @Override
  public boolean offerFirst(E e) {
    deque.add(0, e);
    return true;
  }

  @Override
  public boolean offerLast(E e) {
    return deque.add(e);
  }

  @Override
  public E removeFirst() {
    return deque.remove(0);
  }

  @Override
  public E removeLast() {
    return deque.remove(size() - 1);
  }

  @Override
  public E pollFirst() {
    if (isEmpty()) {
      return null;
    }
    return deque.remove(0);
  }

  @Override
  public E pollLast() {
    if (isEmpty()) {
      return null;
    }
    return deque.remove(size() - 1);
  }

  @Override
  public E getFirst() {
    return deque.get(0);
  }

  @Override
  public E getLast() {
    return deque.get(size() - 1);
  }

  @Override
  public E peekFirst() {
    if (isEmpty()) {
      return null;
    }
    return deque.get(0);
  }

  @Override
  public E peekLast() {
    if (isEmpty()) {
      return null;
    }
    return deque.get(size() - 1);
  }

  @Override
  public boolean offer(E e) {
    return deque.add(e);
  }

  @Override
  public E remove() {
    return deque.remove(0);
  }

  @Override
  public E poll() {
    if (isEmpty()) {
      return null;
    }
    return deque.remove(0);
  }

  @Override
  public E element() {
    return deque.get(0);
  }

  @Override
  public E peek() {
    if (isEmpty()) {
      return null;
    }
    return deque.get(0);
  }

  @Override
  public void push(E e) {
    deque.add(0, e);
  }

  @Override
  public E pop() {
    return deque.remove(0);
  }

  @Override
  public Iterator<E> descendingIterator() {
    LinkedList<E> linkedList = new LinkedList<>(deque);
    return linkedList.descendingIterator();
  }

  @Override
  public boolean removeFirstOccurrence(Object o) {
    boolean isOccurrence = false;
    for (int i = 0; i < deque.size(); i++) {
      if (deque.get(i).equals(o)) {
        isOccurrence = true;
        deque.remove(i);
        break;
      }
    }
    return isOccurrence;
  }

  @Override
  public boolean removeLastOccurrence(Object o) {
    boolean isOccurrence = false;
    for (int i = deque.size() - 1; i >= 0; i--) {
      if (deque.get(i).equals(o)) {
        isOccurrence = true;
        deque.remove(i);
        break;
      }
    }
    return isOccurrence;
  }

  @Override
  public boolean isEmpty() {
    return deque.isEmpty();
  }

  @Override
  public Object[] toArray() {
    return deque.toArray();
  }

  @Override
  public <T> T[] toArray(T[] a) {
    Object[] deqArray = deque.toArray();
    if (a.length < deqArray.length) {
      return (T[]) Arrays.copyOf(deqArray, deqArray.length, a.getClass());
    }
    System.arraycopy(deqArray, 0, a, 0, size());
    if (a.length > deqArray.length) {
      a[deqArray.length] = null;
    }
    return a;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return deque.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    return deque.addAll(c);
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return deque.removeAll(c);
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return deque.retainAll(c);
  }

  @Override
  public void clear() {
    deque.clear();
  }
}
