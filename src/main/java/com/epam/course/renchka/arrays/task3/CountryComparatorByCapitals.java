package com.epam.course.renchka.arrays.task3;

import java.util.Comparator;

public class CountryComparatorByCapitals implements Comparator<Country> {

  @Override
  public int compare(Country o1, Country o2) {
    return o1.getCapital().compareTo(o2.getCapital());
  }
}
