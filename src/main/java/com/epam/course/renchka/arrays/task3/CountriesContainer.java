package com.epam.course.renchka.arrays.task3;

public class CountriesContainer {
  public static final String[][] DATA = {
      {"ALBANIA","Tirana"}, {"AUSTRIA","Vienna"},
      {"BELGIUM","Brussels"}, {"CROATIA","Zagreb"},
      {"CZECH REPUBLIC","Prague"}, {"DENMARK","Copenhagen"},
      {"ESTONIA","Tallinn"}, {"FINLAND","Helsinki"},
      {"FRANCE","Paris"}, {"GERMANY","Berlin"},
      {"GREECE","Athens"}, {"HUNGARY","Budapest"},
      {"IRELAND","Dublin"}, {"ITALY","Rome"},
      {"LATVIA","Riga"}, {"LITHUANIA","Vilnius"},
      {"MONACO","Monaco"}, {"MONTENEGRO","Podgorica"},
      {"THE NETHERLANDS","Amsterdam"}, {"NORWAY","Oslo"},
      {"POLAND","Warsaw"}, {"PORTUGAL","Lisbon"},
      {"ROMANIA","Bucharest"},{"SERBIA","Belgrade"},
      {"SPAIN","Madrid"},  {"LUXEMBOURG","Luxembourg"},
      {"SWEDEN","Stockholm"}, {"UNITED KINGDOM","London"}};
}
