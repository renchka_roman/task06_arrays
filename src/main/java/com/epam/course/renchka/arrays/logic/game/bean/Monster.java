package com.epam.course.renchka.arrays.logic.game.bean;

public class Monster implements Unknown {

  public static final int MAX_POWER = 100;
  public static final int MIN_POWER = 5;
  private int power;

  public Monster(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  @Override
  public int action() {
    return -power;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Monster monster = (Monster) o;
    return power == monster.power;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result += 31 * result + power;
    return result;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName().toUpperCase();
  }
}
