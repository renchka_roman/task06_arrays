package com.epam.course.renchka.arrays.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class StringContainer {

  private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
  private static final int DEFAULT_SIZE = 10;
  private String[] stringArray;
  private int size;

  public StringContainer() {
    this.stringArray = new String[DEFAULT_SIZE];
  }

  public StringContainer(int initialCapacity) {
    if (initialCapacity >= 0) {
      this.stringArray = new String[initialCapacity];
    } else {
      throw new IllegalArgumentException("Illegal Capacity: " + size);
    }
  }

  public boolean add(String str) {
    ensureCapacity(size + 1);
    stringArray[size++] = str;
    return true;
  }

  public String get(int index) {
    if (index < 0 || index > size - 1) {
      throw new IndexOutOfBoundsException(
          "Index: " + index + ", Size: " + size);
    }
    return stringArray[index];
  }

  private void ensureCapacity(int minCapacity) {
    if (minCapacity < 0) {
      throw new OutOfMemoryError();
    }
    if (minCapacity - stringArray.length > 0) {
      grow(minCapacity);
    }
  }

  private void grow(int minCapacity) {
    int newCapacity = 0;
    if (minCapacity < DEFAULT_SIZE) {
      newCapacity = DEFAULT_SIZE;
    } else {
      int oldCapacity = stringArray.length;
      newCapacity = (int) (oldCapacity * 1.5);
      if (newCapacity < 0) {
        newCapacity = minCapacity;
      }
      if (newCapacity - MAX_ARRAY_SIZE > 0) {
        newCapacity = hugeCapacity(minCapacity);
      }
    }
    stringArray = Arrays.copyOf(stringArray, newCapacity);
  }

  private static int hugeCapacity(int minCapacity) {
    return (minCapacity > MAX_ARRAY_SIZE) ?
        Integer.MAX_VALUE :
        MAX_ARRAY_SIZE;
  }

  public static void main(String[] args) {
    StringContainer container = new StringContainer();
    Date begin = new Date();
    for (int i = 0; i < 1000000; i++) {
      container.add("String_1" + i);
    }
    Date end = new Date();
    System.out.println(end.getTime() - begin.getTime());

    List<String> list = new ArrayList<>();
    begin = new Date();
    for (int i = 0; i < 1000000; i++) {
      list.add("String_1" + i);
    }
    end = new Date();
    System.out.println(end.getTime() - begin.getTime());
  }
}
