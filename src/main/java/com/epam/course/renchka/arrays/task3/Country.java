package com.epam.course.renchka.arrays.task3;

public class Country implements Comparable<Country>{
  private String country;
  private String capital;

  public Country(String country, String capital) {
    this.country = country;
    this.capital = capital;
  }

  public String getCountry() {
    return country;
  }

  public String getCapital() {
    return capital;
  }

  @Override
  public int compareTo(Country o) {
    return country.compareTo(o.getCapital());
  }

  @Override
  public String toString() {
    return country + " - " + capital;
  }
}
