package com.epam.course.renchka.arrays.logic.game.bean;

public interface Unknown {

  int getPower();

  int action();
}
