package com.epam.course.renchka.arrays.logic.game;

import com.epam.course.renchka.arrays.logic.game.controller.Controller;
import com.epam.course.renchka.arrays.logic.game.controller.GameController;
import com.epam.course.renchka.arrays.logic.game.model.GameModel;
import com.epam.course.renchka.arrays.logic.game.model.Model;
import com.epam.course.renchka.arrays.logic.game.model.Hall;
import com.epam.course.renchka.arrays.logic.game.view.MyView;
import com.epam.course.renchka.arrays.logic.game.view.View;
import java.io.IOException;

public class Application {

  public static void main(String[] args) throws IOException {
    Model model = new GameModel(new Hall());
    Controller controller = new GameController(model);
    View view = new MyView(controller);
    view.start();
  }
}
