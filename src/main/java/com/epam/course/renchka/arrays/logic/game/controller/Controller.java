package com.epam.course.renchka.arrays.logic.game.controller;

import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Hero;
import java.util.List;

public interface Controller {

  void start();

  List<Door> getClosedDoors();

  Door[] getDoors();

  int getCountOfFatalDoors();

  String openRandomDoor();

  Hero getHero();

  int[] getTheWayToWinGame();
}
