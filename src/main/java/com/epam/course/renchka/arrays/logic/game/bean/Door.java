package com.epam.course.renchka.arrays.logic.game.bean;

public class Door {

  private int id;
  private Unknown unknown;
  private boolean isOpen;

  public Door(int id, Unknown unknown) {
    this.id = id;
    this.unknown = unknown;
  }

  public Unknown open() {
    isOpen = true;
    return unknown;
  }

  public boolean isOpen() {
    return isOpen;
  }

  public Unknown look() {
    return unknown;
  }

  public int getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Door door = (Door) o;
    if (id != door.id) {
      return false;
    }
    return unknown.equals(door.unknown);
  }

  @Override
  public int hashCode() {
    int result = 17;
    result += 31 * result + id;
    result += 31 * result + unknown.hashCode();
    return result;
  }
}
