package com.epam.course.renchka.arrays.logic.game.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

  void print() throws IOException;
}
