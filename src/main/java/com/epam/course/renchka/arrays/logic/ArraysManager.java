package com.epam.course.renchka.arrays.logic;

import java.util.Arrays;
import java.util.Random;

public class ArraysManager {

  private static Random random = new Random();
  private static final int MAX_ELEMENT = 5;
  private static final int MIN_ELEMENT = -5;
  private static final int MAX_SIZE = 20;
  private static final int MIN_SIZE = 19;

  public static void main(String[] args) {
  }

  public static int[] removeDuplicateElementsGoingInARow(int[] array) {
    int sizeOfResultArray = 0;
    int[] temptArray = new int[array.length];
    for (int i = 0; i < array.length - 1; i++) {
      if (array[i] != array[i + 1]) {
        temptArray[sizeOfResultArray++] = array[i];
      }
    }
    temptArray[sizeOfResultArray++] = array[array.length - 1];
    if (temptArray.length > sizeOfResultArray) {
      return cutArrayToLength(temptArray, sizeOfResultArray);
    }
    return temptArray;
  }

  public static int[] removeElementsWhichAppearMoreThanN(int[] array, int n) {
    Arrays.sort(array);
    int[] temporaryArray = new int[array.length];
    int sizeOfResultArray = 0;
    int appearCount = 0;
    for (int i = 1; i < array.length; i++) {
      if (array[i - 1] == array[i]) {
        appearCount++;
      } else {
        if (appearCount < n) {
          while (appearCount >= 0) {
            temporaryArray[sizeOfResultArray++] = array[i - 1];
            appearCount--;
          }
        }
        appearCount = 0;
      }
    }
    if (appearCount < n) {
      while (appearCount >= 0) {
        temporaryArray[sizeOfResultArray++] = array[array.length - 1];
        appearCount--;
      }
    }
    return cutArrayToLength(temporaryArray, sizeOfResultArray);
  }

//  public static int[] removeElementsWhichAppearMoreThanN(int[] array, int n) {
//    Map<Integer, Integer> map = new HashMap<>();
//    for (int number : array) {
//      map.put(number,
//          map.get(number) == null ? 1 : map.get(number) + 1);
//    }
//    int[] tempArray = new int[array.length];
//    int sizeOfResultArray = 0;
//    for (Map.Entry<Integer, Integer> pair : map.entrySet()) {
//      int appearCount = pair.getValue();
//      if (appearCount <= n) {
//        while (appearCount > 0) {
//          tempArray[sizeOfResultArray++] = pair.getKey();
//          appearCount--;
//        }
//      }
//    }
//    if (tempArray.length > sizeOfResultArray) {
//      int[] ar = cutArrayToLength(tempArray, sizeOfResultArray);
//      Arrays.sort(ar);
//      return ar;
//    }
//    return tempArray;
//  }

  public static int[] getElementsPresentInFirstAndNotInSecond(
      int[] firstArray, int[] secondArray) {
    Arrays.sort(firstArray);
    Arrays.sort(secondArray);
    int[] resultArray = new int[firstArray.length];
    int sizeOfResultArray = 0;
    for (int i : firstArray) {
      boolean isPresent = false;
      for (int j : secondArray) {
        if (i == j) {
          isPresent = true;
          break;
        } else if (i < j) {
          break;
        }
      }
      if (!isPresent) {
        resultArray[sizeOfResultArray++] = i;
      }
    }
    return cutArrayToLength(resultArray, sizeOfResultArray);
  }

  public static int[] getArrayOfCommonElements(
      int[] firstArray, int[] secondArray) {
    int size = firstArray.length;
    if (firstArray.length > secondArray.length) {
      size = secondArray.length;
    }
    int[] tempArray = new int[size];
    Arrays.sort(firstArray);
    Arrays.sort(secondArray);

    int sizeOfResultArray = 0;
    int i = 0, j = 0;
    while (i < firstArray.length && j < secondArray.length) {
      if (firstArray[i] == secondArray[j]) {
        tempArray[sizeOfResultArray] = firstArray[i];
        i++;
        j++;
        sizeOfResultArray++;
      } else if (firstArray[i] < secondArray[j]) {
        i++;
      } else {
        j++;
      }
    }
    if (tempArray.length > sizeOfResultArray) {
      return cutArrayToLength(tempArray, sizeOfResultArray);
    }
    return tempArray;
  }

  public static int[] createRandomArray(int size) {
    int[] array = new int[size];
    for (int i = 0; i < size; i++) {
      array[i] =
          random.nextInt(MAX_ELEMENT - MIN_ELEMENT) + MIN_ELEMENT;
    }
    return array;
  }

  public static int[] createRandomArray() {
    int size = random.nextInt(MAX_SIZE - MIN_SIZE) + MIN_SIZE;
    return createRandomArray(size);
  }

  public static int[] cutArrayToLength(int[] array, int length) {
    int[] resultArray = new int[length];
    System.arraycopy(array, 0, resultArray, 0, length);
    return resultArray;
  }

  public static void printArray(int[] array) {
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println();
  }
}
