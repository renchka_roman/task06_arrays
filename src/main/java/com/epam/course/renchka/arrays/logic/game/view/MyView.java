package com.epam.course.renchka.arrays.logic.game.view;

import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Unknown;
import com.epam.course.renchka.arrays.logic.game.controller.Controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView implements View {

  private static Logger log = LogManager.getLogger(MyView.class);
  private static Map<String, Printable> menuMethods = new HashMap<>();
  private static Map<String, String> menu = new LinkedHashMap<>();

  static {
    menu.put("1", " 1 - Open random door.");
    menu.put("2", " 2 - Show information about doors.");
    menu.put("3", " 3 - Print count the doors can kill you.");
    menu.put("4", " 4 - Print door list in that order you need to open.");
    menu.put("Q", " Q - Quit.");
  }

  private Controller controller;
  private BufferedReader reader =
      new BufferedReader(new InputStreamReader(System.in));

  public MyView(Controller controller) {
    this.controller = controller;
    menuMethods.put("1", this::openRandomDoor);
    menuMethods.put("2", this::printInfoAboutTheDoors);
    menuMethods.put("3", this::printCountOfFatalDoors);
    menuMethods.put("4", this::printDoorsInThatOrderYouNeedToOpen);
  }

  @Override
  public void start() throws IOException {
    System.out.println("LET'S START THE GAME!");
    printSeparate();
    System.out.println("Put any key to fill the doors.");
    String str = null;
    do {
      str = reader.readLine();
    } while (str == null);
    controller.start();
    show();
  }

  private void printMenu() {
    printSeparate();
    System.out.println("Your power: " + controller.getHero().getPower());
    System.out.println("Actions:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
    printSeparate();
  }

  public void show() throws IOException {
    String keyMenu;
    do {
      printMenu();
      System.out.println("Please, select menu point.");
      keyMenu = reader.readLine().toUpperCase();
      Printable printable = menuMethods.get(keyMenu);
      if (printable != null) {
        printable.print();
      }
    } while (!keyMenu.equals("Q")
        && controller.getHero().getPower() >= 0
        && controller.getClosedDoors().size() > 0);
  }

  @Override
  public void printInfoAboutTheDoors() {
    Door[] doors = controller.getDoors();
    printSeparate();
    System.out.println("We are waiting for you behind the doors:");
    printSeparate();
    System.out.println(
        String.format("\t%-5s %-10s %-10s %s",
            "Door", "Status", "Item:", "Points of Power:"));
    System.out.println(
        String.format("\t%-5s %-10s %-10s %s",
            "----", "-----", "-----", "---------------"));
    for (Door door : doors) {
      Unknown unknown = door.look();
      String status = door.isOpen() ? "Open" : "Closed";
      System.out.println(
          String.format("\t%-5s %-10s %-10s %s",
              door.getId(), status, unknown, unknown.getPower()));
    }
  }

  @Override
  public void printCountOfFatalDoors() {
    printSeparate();
    System.out.println("Count of the doors that can kill you: "
        + controller.getCountOfFatalDoors());
  }

  @Override
  public void openRandomDoor() {
    printSeparate();
    System.out.println(controller.openRandomDoor());
  }

  @Override
  public void printDoorsInThatOrderYouNeedToOpen() {
    int[] wayToWin = controller.getTheWayToWinGame();
    if (wayToWin.length == 0) {
      printSeparate();
      System.out.println("It's impossible to win.");
    } else {
      StringBuilder builder = new StringBuilder();
      for (int i : wayToWin) {
        builder.append(i);
        builder.append(" -> ");
      }
      System.out.println(builder.toString());
    }
  }

  private void printSeparate() {
    System.out.println("-------------------------------------------------");
  }
}
