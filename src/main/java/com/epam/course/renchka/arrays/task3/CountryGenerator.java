package com.epam.course.renchka.arrays.task3;

import static com.epam.course.renchka.arrays.task3.CountriesContainer.DATA;

import java.util.Random;

public class CountryGenerator implements Generator<Country> {
  private static Random random = new Random();

  @Override
  public Country next() {
    String[] country = DATA[random.nextInt(DATA.length)];
    return new Country(country[0], country[1]);
  }
}
