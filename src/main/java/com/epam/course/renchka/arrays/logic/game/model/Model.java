package com.epam.course.renchka.arrays.logic.game.model;

import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Hero;
import java.util.List;

public interface Model {

  void fillDoors();

  int getCountOfFatalDoors();

  Door[] getDoors();

  List<Door> getClosedDoors();

  String openRandomDoor();

  Hero getHero();

  int[] getTheWayToWinGame();
}
