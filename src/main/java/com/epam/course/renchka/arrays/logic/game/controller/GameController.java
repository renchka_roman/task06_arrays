package com.epam.course.renchka.arrays.logic.game.controller;

import com.epam.course.renchka.arrays.logic.game.bean.Door;
import com.epam.course.renchka.arrays.logic.game.bean.Hero;
import com.epam.course.renchka.arrays.logic.game.model.Model;
import java.util.List;

public class GameController implements Controller {

  private Model model;

  public GameController(Model model) {
    this.model = model;
  }

  @Override
  public void start() {
    model.fillDoors();
  }

  @Override
  public Door[] getDoors() {
    return model.getDoors();
  }

  @Override
  public int getCountOfFatalDoors() {
    return model.getCountOfFatalDoors();
  }

  @Override
  public Hero getHero() {
    return model.getHero();
  }

  @Override
  public String openRandomDoor() {
    return model.openRandomDoor();
  }

  @Override
  public List<Door> getClosedDoors() {
    return model.getClosedDoors();
  }

  @Override
  public int[] getTheWayToWinGame() {
    return model.getTheWayToWinGame();
  }
}
